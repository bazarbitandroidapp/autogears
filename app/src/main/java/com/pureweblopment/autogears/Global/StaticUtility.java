package com.pureweblopment.autogears.Global;

/**
 * Created by divya on 4/9/17.
 */

public class StaticUtility {

    //Local URL
    /*public static String URL = "http://192.168.0.110/bazzarbit_api/";*/

    //Fancy Shop URL
    /*public static String URL = "http://192.168.0.110/bazarbit-QA/bazzarbit_api/";*/

    //public static String URL = "http://192.168.0.112/bazarbit/bazzarbit_api/";

    /*public static String URL = "http://192.168.0.111/bazzarbit/bazarbit-stable/bazzarbit_api/";*/

    //Live URL
    public static String URL = "http://bazarbit.com/bazzarbit/";

    //Live URL
//    public static String URL = "http://52.66.153.88/bazzarbit/";

    public static String Checkregister = "app/frontend/checksocialdetails";
    public static String Registration = "app/frontend/register";
    public static String ForgotPWD = "app/frontend/forgotpassword";
    public static String Login = "app/frontend/login";
    public static String Logout = "app/frontend/logout";
    public static String getCountryCode = "countrycodes";
    public static String ApplicationSettings = "app/frontend/getapplicationsettings";
    public static String AppSetting = "app/frontend/getappsetting";
    public static String GETSLIDER = "app/frontend/getslider";
    public static String getCategory = "app/frontend/categories";
    public static String getBestSeller = "app/frontend/bestSeller";
    public static String getWantedProduct = "app/frontend/mostwantedproducts";
    public static String getNewArrivals = "app/frontend/newarrivals";
    public static String getBenner = "app/frontend/getbanner";
    public static String UPDATE_CART_BY_USERID_AFTER_LOGIN = "app/frontend/user/updatecartsession";
    public static String AddToWishlist = "app/frontend/user/addtowishlist";
    public static String GetProductList = "app/frontend/getcatwiseproducts";
    public static String CheckWishlist = "app/frontend/user/checkiteminwishlist";
    public static String SortByAttribute = "app/frontend/getattributessorting";
    public static String GetSigalProduct = "app/frontend/getproductdetailbyslug";
    public static String SubmitInquiryData = "app/frontend/productinquiry";
    public static String GetCheckPincode = "app/frontend/checkpincodeexists";
    public static String RelatedProduct = "app/frontend/relatedProducts";
    public static String GetReviews = "app/frontend/reviewdetail/gets";
    public static String CheckItemINCart = "app/frontend/checkitemincart";
    public static String AddToCart = "app/frontend/addtocart";
    public static String CartCount = "app/frontend/cartcount";
    public static String NotifyMe = "app/frontend/user/notifyme";
    public static String CheckNotifyMe = "app/frontend/user/checkiteminnotifyme";
    public static String GetPayUMoneyCredential = "app/frontend/getpayumoneycredential";
    public static String GetCartItems = "app/frontend/getcartitems";
    public static String GetQauntyIncreseDecrese = "app/frontend/updatecartitemqty";
    public static String RemoveCart = "app/frontend/removecartitem";
    public static String LastOrderAddress = "app/frontend/user/getlastorderaddress";
    public static String Countries = "countries";
    public static String States = "states";
    public static String ADDAddresses = "app/frontend/user/myaccount/adduseraddress";
    public static String AddressList = "app/frontend/user/myaccount/useraddresslist";
    public static String DeleteAddress = "app/frontend/user/myaccount/deleteuseraddress";
    public static String EditAddress = "app/frontend/user/myaccount/updateuseraddress";
    public static String ApplyPromocode = "app/frontend/validatepromocode";
    public static String Bingage_Member_Info = "app/frontend/user/transaction/getmemberinfo";
    public static String RemovePromocode = "app/frontend/removepromocode";
    /*public static String GetPaymentGetway = "app/frontend/getpaymentgateway";*/
    public static String GetPaymentGateway = "app/frontend/getpaymentgatewaylist";
    public static String Getshipping_charge_Delivery = "app/frontend/getshippingcharges";
    public static String GetCartTotal = "app/frontend/getcarttotal";
    public static String PlaceOrder = "app/frontend/user/transaction/placeorder";
    public static String GET_PAYPAL_CLENT_TOKEN = "app/get-braintree-client-token";
    public static String Wishlist = "app/frontend/user/getwishlistitems";
    public static String RemoveWishlist = "app/frontend/user/removeWishlistItem";
    public static String OrderHistory = "app/frontend/user/getorders";
    public static String SingleOrderDetail = "app/frontend/user/getsingleorder";
    public static String CancelOrder = "app/frontend/user/updateitemstatus";
    public static String ReturnCODOrder = "app/frontend/user/returnorder";
    public static String CancelOnlineOrder = "app/frontend/user/updateitemstatus";
    public static String ReturnNetOrder = "app/frontend/user/returnnetorder";
    public static String SubmitReview = "app/frontend/reviewdetail/create";
    public static String ALLReviews = "app/frontend/user/myaccount/listreview";
    public static String GetLastUserInfo = "app/frontend/user/myaccount/getbyid";
    public static String UpdateProfile = "app/frontend/user/myaccount/update";
    public static String GetFeedbackForm = "app/frontend/feedbackform";
    public static String SubmitFeedback = "app/frontend/user/submitfeedback";
    public static String GetCMSListing = "app/frontend/cmslist";
    public static String CMSStaticPage = "app/frontend/cmsstaticpage";
    public static String ChangePWD = "app/frontend/user/myaccount/changepassword";
    public static String GetSearchResult = "app/frontend/searchproductlist";
    public static String DisableVariationOption = "app/frontend/disabledvariationoption";
    public static String GetVariationData = "app/frontend/productvariation";
    public static String GetDefualtVariationData = "app/frontend/getdefaultvariationbyproductid";
    public static String GetMegaMenu = "app/menu/getRows";
    public static String Feedback = "app/frontend/user/getdeliveredorders";
    public static String SubmitContactUs = "app/frontend/createcontactform";
    public static String verifyOTP = "app/frontend/verifyotp";
    public static String ResendOTP = "app/frontend/resendotp";
    public static String SendOTP = "app/frontend/user/transaction/sendotp";
    public static String PlaceOrderVerifyOTP = "app/frontend/user/transaction/verifyotp";
    public static String Redeem_Wallet = "app/frontend/user/transaction/redeembingagewallet";
    public static String Bingage_verify_otp = "app/frontend/user/transaction/bingageverifyotp";
    public static String Send_Otp_Bingage = "app/frontend/user/transaction/bingageresendotp";
    public static String FeatureSetting = "app/frontend/getfeaturessettings";
    public static String GetCurrencyList = "app/frontend/getcurrencylist";
    public static String GetMultiLanguageDate = "app/frontend/getlanguagedata";
    public static String GetPromocodeList = "app/frontend/promocodelist";
    public static String GetCommission = "app/frontend/user/getCommission";
    public static String RemovePreBooking = "app/frontend/removeprebooking";
    public static String PaymentStatusUpdate = "app/frontend/transaction/update-payment";

    //region STRINGS FOR USE IN NOTIFICATION...
    public static final String NOTIFICATION_LEGACY_SERVER_KEY = "AIzaSyB9fWESmF0F_GTpnqctUBuPDgYXnqG7IOs";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String GCM_TOKEN = "gcm_token";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    //endregion

    public static final String CLIENT_ID = "02090aa093f24a0695f3f0eab1134016";
    public static final String CLIENT_SECRET = "fd9a2c34417b476bb42d0dc9cf9bd186";
    public static final String CALLBACK_URL = "http://192.168.0.212/bazzarbitFront/index.php";


    public static final String LinkedinCLIENT_ID = "81va8uw7fll2ey";
    public static final String LinkedinCLIENT_SECRET = "x9QzbF8Zkidi5pLD";

    public static final String PinterestApp_ID = "4942597597022925280";
    public static final String PinteresApp_SECRET = "13f706620cf85f8893a44efc49bb93dc8104c342ee66f21e929edb2bb10d51b6";

    public static final String ThemePrimaryColor = "themeprimarycolor";
    public static final String ThemePrimaryDarkColor = "themeprimarydarkcolor";
    public static final String TextColor = "textcolor";
    public static final String TextLightColor = "textlightcolor";
    public static final String ButtonTextColor = "btntextcolor";

    public static final String IMG_LOGO = "imglogo";
    public static final String IMG_Facebook = "imgFacebook";
    public static final String IMG_google = "imggoogle";
    public static final String IMG_Linkedin = "imglinkedin";
    public static final String IMG_pinterest = "imgpinterest";
    public static final String IMG_insta = "imginsta";
    public static final String APP_LOGO = "applogo";
    public static final String Feature_List = "featurelist";

    public static final String Is_Slider_Active = "is_slider";
    public static final String Is_Cart_Active = "is_cart";
    public static final String Is_Order_Active = "is_order";
    public static final String Is_Wishlist_Active = "is_wishlist";
    public static final String Is_Arrival_Active = "is_arrival";
    public static final String Is_Seller_Active = "is_seller";
    public static final String Is_Mostwanted_Active = "is_mostwanted";
    public static final String Is_MEGAMENU_Active = "is_megamenu_active";
    public static final String ALLOW_OTP_VERIFICATION = "allow_otp_verification";
    public static final String IS_PRODUCTINQUIRY_ACTIVE = "is_productinquiry_active";
    public static final String CHECK_PINCODE_AVAILABILITY = "check_pincode_availibility";
    public static final String IS_MULTI_CURRENCY_ACTIVE = "is_multicurrency_active";
    public static final String REF_CODE_ENABLED = "ref_code_enabled";
    public static final String PRE_BOOKING_ENABLED = "pre_booking_enabled";


    public static final String sCurrencyName = "currencyname";
    public static final String sCurrencySign = "currencysign";
    public static final String sCurrencySignPosition = "currencysignposition";
    public static final String sCurrencyCode = "currencycode";

    public static final String IsSelectAddress = "isSelectAddress";
    public static final String strShippingUserName = "strshippingusername";
    public static final String strShippingAddress = "strshippingaddresss";
    public static final String strShippingPhoneno = "strshippingphoneno";
    public static final String strPincode = "strPincode";
    public static final String strShippingFname = "strshippingfname";
    public static final String strShippingLname = "strshippinglname";
    public static final String strShippingAddress1 = "strshippingaddress";
    public static final String strShippingLandmark = "strshippinglandmark";
    public static final String strShippingCountry = "strshippingcounty";
    public static final String strShippingCity = "strshippingcity";
    public static final String strShippingState = "strshippingState";

    public static final String strBillingUserName = "strbillingusername";
    public static final String strBillingAddress = "strbillingaddresss";
    public static final String strBillingPhoneno = "strbillingphoneno";
    public static final String strBillingPincode = "strbillingPincode";
    public static final String strBillingFname = "strbillingfname";
    public static final String strBillingLname = "strbillinglname";
    public static final String strBillingAddress1 = "strbillingaddress";
    public static final String strBillingLandmark = "strbillinglandmark";
    public static final String strBillingCountry = "strbillingcounty";
    public static final String strBillingCity = "strbillingcity";
    public static final String strBillingState = "strbillingState";

    public static final String strTotalPrice = "totalPrice";
    public static final String strTotalItems = "totalitems";
    public static final String sPrebookingID = "prebookingid";
    public static final String PREFERENCEOrderHistory = "Preferenceorderhistory";
    public static final String strOrderHistoryLastClick = "orderhistorylastclick";

    public static final String strIscheck = "ischeck";
    public static final String strIsPromocode = "ispromocode";
    public static final String WishlistCount = "wishlistcount";
    public static final String UserProfile = "userprofile";
    public static final String FullName = "fullname";
    public static final String ArrayAttribut = "arrayattribute";
    public static final String ArrayAttributTerm = "arrayattributeterm";

    public static final String Fromdate = "userprofile";
    public static final String sPincode = "pincode";

    //Currency screen visibility
    public static String sCURRENCYSCREENVISIBILITY = "no";


    //Multi Language
    public static final String MULTILANGUAGEPREFERENCE = "multilanguagepreference";
    public static final String sSUCCESS_ORDER = "success_order";
    public static final String sTHANK_YOU_ORDER = "thank_you_order";
    public static final String sTHANK_YOU_RESPONSE = "thank_you_response";
    public static final String sCONTINUE_SHOPPING = "continue_shopping";
    public static final String sEXPEXCTED_DELIVERY = "expected_delivery";
    public static final String sDAYS_RETURN_AVAILABLE = "days_return_available";
    public static final String sREAD_MORE = "read_more";
    public static final String sNOTIFY_ME = "notify_me";
    public static final String sSELECT = "select";
    public static final String sOUT_OF_STOCK = "out_of_stock";
    public static final String sGO_TO_CART = "go_to_cart";
    public static final String sCHECK_DELIVERY_OPTION = "check_delivery_option";
    public static final String sCHECK = "check";
    public static final String sPRODUCT_DETAILS = "product_details";
    public static final String sRELATED_PRODUCT = "related_product";
    public static final String sENTER_PINCODE = "enter_pincode";
    public static final String sPOPULAR_REVIEWS = "popular_reviews";
    public static final String sADD_TO_WISHLIST = "add_to_wishlist";
    public static final String sADD_TO_CART = "add_to_cart";
    public static final String sCHECK_DELIVERY_OPTION_AVAILABLE_OR_NOT =
            "check_delivery_option_available_or_not";
    public static final String sENTER_ANOTHER_PINCODE = "enter_another_pincode";
    public static final String sDELIVERY_AVAILABLE = "delivery_available";
    public static final String sDELIVERY_NOT_AVAILABLE = "delivery_not_available";
    public static final String sPINCODE_REQUIRED = "pincode_required";
    public static final String sFILTER = "filter";
    public static final String sAPPLY = "apply";
    public static final String sCLEAR = "clear";
    public static final String sNO_RECORD_FOUND = "no_record_found";
    public static final String sARE_YOU_SURE_YOU_WANT_TO_EXIT = "are_you_sure_you want_to_exit";
    public static final String sARE_YOU_SURE_YOU_WANT_TO_CLEAR = "are_you_sure_you want_to_clear";
    public static final String sWISHLIST_EMPTY = "wishlist_empty";
    public static final String sVIEW_WISHLIST_DETAIL = "view_wishlist_detail";
    public static final String sREMOVE_FROM_WISHLIST = "remove_from_wishlist";
    public static final String sDATE_OFF = "data_off";
    public static final String sTURN_ON_DATA_WIFI = "turn_on_data_wifi";
    public static final String sSETTINGS = "settings";
    public static final String sEXIT = "exit";
    public static final String sEDIT = "edit";
    public static final String sDELETE = "Delete";
    public static final String sUPDATE_LINKEDIN = "update_linkedin";
    public static final String sCONNECT_LINKEDIN = "connect_linkedin";
    public static final String sDOWNLOAD = "download";
    public static final String sCANCEL = "cancel";
    public static final String sPROCESS = "processing";
    public static final String sPLEASE_WAIT = "please_wait";
    public static final String sMY_ADDRESS = "my_address";
    public static final String sFIREST_NAME = "first_name";
    public static final String sLAST_NAME = "last_name";
    public static final String sADDRESS = "address";
    public static final String sLANDMARK = "landmark";
    public static final String sPINCODE = "pincode";
    public static final String sSELECT_COUNTRY = "select_country";
    public static final String sSELECT_STATE = "select_state";
    public static final String sCITY = "city";
    public static final String sCODE = "code";
    public static final String sPHONE_NUMBER = "phone_number";
    public static final String sIS_SHIPPING = "is_shipping";
    public static final String sADD_ADDRESS = "add_address";
    public static final String sOLD_PASSWORD = "old_password";
    public static final String sNEW_PASSWORD = "new_password";
    public static final String sCONFIRM_PASSWORD = "confirm_password";
    public static final String sCHANGE_PASSWORD = "change_password";
    public static final String sPAYMENT = "payment";
    public static final String sSUCCESS = "success";
    public static final String sNO_ADDRESS_AVAILABLE = "no_address_available";
    public static final String sSELECT_CURRENCY = "select_currency";
    public static final String sCURRENCY = "currency";
    public static final String sDONE = "done";
    public static final String sSKIP = "skip";
    public static final String sUPDATE = "update";
    public static final String sFORGOT_PASSWORD = "forgot_password";
    public static final String sLOG_IN_NOW = "log_in_now";
    public static final String sEMAIL_ADDRESS = "email_address";
    public static final String sPASSWORD = "password";
    public static final String sFORGOT_YOUR_PASSWORD = "forgot_your_password";
    public static final String sLOG_IN = "log_in";
    public static final String sNOT_HAVE_AN_ACCOUNT = "not_have_an_account";
    public static final String sSIGN_UP_NOW = "sign_up_now";
    public static final String sLOGOUT = "logout";
    public static final String sSEACRCH_BEST_PRODUCT = "search_best_product";
    public static final String sOUR_BEST_SELLER = "our_best_seller";
    public static final String sMOST_WANTED = "most_wanted";
    public static final String sNEW_ARRIVALS = "new_arrivals";
    public static final String sHOME = "home";
    public static final String sCATEGORY = "category";
    public static final String sNOTIFICATION = "notification";
    public static final String sWISHLIST = "wishlist";
    public static final String sPROFILE = "profile";
    public static final String sGUEST_EMAIL_ADDRESS = "guest_email_address";
    public static final String sGUEST_USER = "guest_user";
    public static final String sNO_ORDER_AVAILABLE = "no_order_available";
    public static final String sFROM_DATE = "from_date";
    public static final String sTO_DATE = "to_date";
    public static final String sDELIVERED = "delivered";
    public static final String sCANCELLED_ORDER = "cancelled_order";
    public static final String sPENDING_ORDER = "pending_order";
    public static final String sCONFIRM_ORDER = "confirm_order";
    public static final String sIN_PROCESS = "in_process";
    public static final String sOUT_FOR_DELIVERY = "out_for_delivery";
    public static final String sRETURN_ORDER = "return_order";
    public static final String sREFUND_ORDER = "refund_order";
    public static final String sRETURNED_ORDER = "returned_order";
    public static final String sORDER_ID = "order_id";
    public static final String sPLEASE_TYPE_VERIFICATION_CODE_AND_SEND_TO =
            "please_type_verification_code_and_send_to";
    public static final String sVERIFICATION_CODE = "verification_code";
    public static final String sNOT_RECEIVE_OTP = "not_receive_the_otp";
    public static final String sCONTINUE = "continue";
    public static final String sRESEND_AGAIN = "resend_again";
    public static final String sENTER_YOUR_OTP = "enter_your_otp";
    public static final String sVERIFY = "verify";
    public static final String sGENDER = "gender";
    public static final String sMALE = "male";
    public static final String sFEMALE = "female";
    public static final String sDATE_OF_BIRTH = "date_of_birth";
    public static final String sSUBMIT = "submit";
    public static final String sMOBILE_NO = "mobile_no";
    public static final String sSIGN_UP = "sign_up";
    public static final String sNO_REVIEW_AVAILABLE = "no_review_available";
    public static final String sPOWERED_BY_BAZRBIT = "powered_by_bazarBit";
    public static final String sSHOPPING_CART = "shopping_cart";
    public static final String sSHOPPING_BAG_IS_EMPTY = "shopping_bag_is_empty";
    public static final String sSTART_SHOPPING_NOW = "start_shopping_now";
    public static final String sCHECKOUT = "checkout";
    public static final String sREMOVE_THIS_PRODUCT_FROM_CART = "remove_this_product_from_cart";
    public static final String sADD = "add";
    public static final String sCHANGE = "change";
    public static final String sYOU_HAVE_ANY_PROMOCODE = "you_have_any_promocode";
    public static final String sPROMO_CODE = "promo_code";
    public static final String sCASH_ON_DELIVERY = "cash_on_delivery";
    public static final String sPAY_U_MONEY = "pay_u_money";
    public static final String sCCAVENUE = "ccavenue";
    public static final String sPAYPAL = "paypal";
    public static final String sSUB_TOTAL = "sub_total";
    public static final String sDISCOUNT = "Discount";
    public static final String sSHIPPING_CHARGES = "shipping_charges";
    public static final String sCOD_CHARGES = "cod_charges";
    public static final String sTOTAL = "total";
    public static final String sNEXT_PAYMENT_INFO = "next_payment_info";
    public static final String sQUANTITY = "quantity";
    public static final String sCONFIRM_PURCHASE = "confirm_purchase";
    public static final String sSEND_OTP = "send_otp";
    public static final String sRESEND = "resend";
    public static final String sQTY = "qty";
    public static final String sCHOOSE_FROM_PROMOCODE_BELOW = "choose_from_promocode_blow";
    public static final String sPLACE_ORDER = "place_order";
    public static final String sSHIPPING_ADDRESS = "shipping_address";
    public static final String sSAME_AS_SHIPPING_ADDRESS = "same_as_shipping_address";
    public static final String sBILLING_ADDRESS = "billing_address";
    public static final String sENTER_PROMO_CODE = "enter_promo_code";
    public static final String sAPPLY_PROMOCODE = "apply_promocode";
    public static final String sCANCEL_ORDER = "cancel_order";
    public static final String sCONTACT_US = "contact_us";
    public static final String sNAME = "name";
    public static final String sSUBJECT = "subject";
    public static final String sMESSAGE = "message";
    public static final String sFEEDBACK = "feedback";
    public static final String sANS = "ans";
    public static final String sQ = "q";
    public static final String sMY_ACCOUNT = "my_account";
    public static final String sORDER_HISTORY = "order_history";
    public static final String sREVIEW = "review";
    public static final String sORDER_DETAIL = "order_detail";
    public static final String sORDER_DATE = "order_date";
    public static final String sORDER_TOTAL = "order_total";
    public static final String sPAYMENT_INFORMATION = "payment_information";
    public static final String sITEM_TOTAL = "item_total";
    public static final String sSURE_CANCEL_THIS_ORDER = "sure_cancel_this_order";
    public static final String sADD_YOUR_REVIEW = "add_your_review";
    public static final String sWOULD_YOU_LIKE_TO_RECOMMEND_THIS_PRODUCT =
            "would_you_like_to_recommend_this_product";
    public static final String sUSER_NAME = "user_name";
    public static final String sTITLE = "title";
    public static final String sDESCRIPTION = "description";
    public static final String sRETURN_PRODUCT = "return_product";
    public static final String sRETURN_DETAIL = "return_detail";
    public static final String sACCOUNT_HOLDER_NAME = "account_holder_name";
    public static final String sBANK_NAME = "bank_name";
    public static final String sBRANCH_NAME = "branch_name";
    public static final String sACCOUNT_NUMBER = "account_number";
    public static final String sIFSC_CODE = "ifsc_code";
    public static final String sREASON = "reason";
    public static final String sYES = "yes";
    public static final String sNO = "no";
    public static final String sITEMS = "items";
    public static final String sREFERRAL_CODE = "referral_code";
    public static final String sPRE_BOOKING = "pre_booking";
    public static final String sCOMMISSION = "commission";
    public static final String sCOMMISSION_PERCENTAGE = "commission_percentage";
    public static final String sCOMMISSION_AMOUNT = "commission_amount";
    public static final String sLOGIN_REQUIRED = "login_required";
    public static final String sREAD_LESS = "read_less";
    public static final String sPENDING = "pending";
    public static final String sREJECTED = "rejected";
    public static final String sDAYS = "days";
    public static final String sCHECK_PINCODE_AVAILABILITY = "check_pincode_availibility";
    public static final String sCHECK_PINCODE_ERROR = "check_pincode_error";
    public static final String sSELECT_SHARE_TYPE = "select_share_type";
    public static final String sSELECT_IMAGE = "select_image";
    public static final String sTAKE_PHOTO = "take_photo";
    public static final String sGALLERY = "gallery";
    public static final String sFACEBOOK = "facebook";
    public static final String sTWITTER = "twitter";
    public static final String sGOOGLE = "google";
    public static final String sSKYPE = "skype";
    public static final String sLINKEDIN = "linkedin";
    public static final String sWHATSAPP = "whatsapp";
    public static final String sPINTEREST = "pinterest";
    public static final String sPASSWORD_LIMIT_VALID = "password_limit_valid";
    public static final String sPASSWORD_CHECK_EMPTY = "password_check_empty";
    public static final String sCONFIRM_PASSWORD_LIMIT_VALID = "confirm_password_limit_valid";
    public static final String sCONFIRM_PASSWORD_CHECK_EMPTY = "confirm_password_check_empty";
    public static final String sCONFIRM_PASSWORD_CHECK_MATCH = "confirm_password_check_match";
    public static final String sOLD_PASSWORD_CHECK_EMPTY = "old_password_check_empty";
    public static final String sNEW_PASSWORD_CHECK_EMPTY = "new_password_check_empty";
    public static final String sEMAIL_CHECK_VALID = "email_check_valid";
    public static final String sEMAIL_CHECK_EMPTY = "email_check_empty";
    public static final String sFIRSTNAME_CHECK_EMPTY = "firstname_check_empty";
    public static final String sLASTNAME_CHECK_EMPTY = "lastname_check_empty";
    public static final String sADDRESS_CHECK_EMPTY = "address_check_empty";
    public static final String sLANDMARK_CHECK_EMPTY = "landmark_check_empty";
    public static final String sPINCODE_CHECK_EMPTY = "pincode_check_empty";
    public static final String sCITY_CHECK_EMPTY = "city_check_empty";
    public static final String sPHONE_CHECK_EMPTY = "phone_check_empty";
    public static final String sPHONE_CHECK_VALID = "phone_check_valid";
    public static final String sCOUNTRY_CHECK_EMPTY = "country_check_empty";
    public static final String sSTATE_CHECK_EMPTY = "state_check_empty";
    public static final String sDOB_CHECK_EMPTY = "dob_check_empty";
    public static final String sANS_CHECK_EMPTY = "ans_check_empty";
    public static final String sRATING_CHECK_EMPTY = "rating_check_empty";
    public static final String sDESC_CHECK_EMPTY = "desc_check_empty";
    public static final String sTITLE_CHECK_EMPTY = "title_check_empty";
    public static final String sUNM_CHECK_EMPTY = "unm_check_empty";
    public static final String sREASON_CHECK_EMPTY = "reason_check_empty";
    public static final String sIFSC_CHECK_EMPTY = "ifsc_check_empty";
    public static final String sACCNO_CHECK_EMPTY = "accno_check_empty";
    public static final String sBANKNAME_CHECK_EMPTY = "bankname_check_empty";
    public static final String sBRANCH_CHECK_EMPTY = "branch_check_empty";
    public static final String sACC_HOLD_NAME_CHECK_EMPTY = "acc_hold_name_check_empty";
    public static final String sSUBJECT_CHECK_EMPTY = "subject_check_empty";
    public static final String sMSG_CHECK_EMPTY = "msg_check_empty";
    public static final String sPWD_NEW_PWD_CHECK_EMPTY = "pwd_newpwd_check_match";
    public static final String sNO_MORE_COMMISSION_CHECK_EMPTY = "no_more_commission_found";
    public static final String sPLEASE_ENTER_OTP = "please_enter_otp";
    public static final String sPLEASE_SELECT_PAYMENT_GATEWAY = "please_select_payment_gateway";
    public static final String sbingage_wallet_error = "bingage_wallet_error";
    public static final String sredeem_from_wallet = "redeem_from_wallet";
    public static final String wallet_balance = "wallet_balance";
    public static final String sPRODUCT_INQUIRY = "product_inquiry";
    public static final String sComment = "comment";

    public static final String PREFERENCEREFERRALCODE = "preferencereferralcode";
    public static final String sREFERRALCODE = "referralcode";

    //Payment Gateway Share Preference
    public static final String PREFERENCEPAYMENTGATETWAY = "preferencepaymentgateway";
    public static final String sCODAVAILABLE = "codavailable";
    public static final String sPAYMENTGATEWAY = "paymentgateway";

    //Paypal Client id :-
    public static final String PAYPAL_CLIENT_ID = "AYsITv3s-2CnU76AuWs7gAsVbiuW88lBOSVbdfPZ3MdfRGsdNaGmLPLLlubIdihRsXpC1plvID43C-6_";

    //region For bingage param
    public static String transactionIdOtp = "transactionid";
    public static String verify_bingage_otp = "verify_bingage_otp";
    public static String Currency_Code = "currencyCode";

    public static final String ACCESS_TOKEN = "access-token";

}
