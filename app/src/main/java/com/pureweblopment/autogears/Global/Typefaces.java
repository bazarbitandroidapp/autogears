package com.pureweblopment.autogears.Global;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by divya on 7/9/17.
 */

public class Typefaces {

    public static Typeface TypefaceCalibri_Regular(Context context) {
        return Typeface.createFromAsset(context.getAssets(),
                "Fonts/Calibri/calibri.ttf");
    }
    public static Typeface TypefaceCalibri_bold(Context context) {
        return Typeface.createFromAsset(context.getAssets(),
                "Fonts/Calibri/calibrib.ttf");
    }
    public static Typeface TypefaceCalibri_italic(Context context) {
        return Typeface.createFromAsset(context.getAssets(),
                "Fonts/Calibri/calibrii.ttf");
    }
    public static Typeface TypefaceCalibri_large(Context context) {
        return Typeface.createFromAsset(context.getAssets(),
                "Fonts/Calibri/calibril.ttf");
    }
    public static Typeface TypefaceCalibri_largeItalic(Context context) {
        return Typeface.createFromAsset(context.getAssets(),
                "Fonts/Calibri/calibrili.ttf");
    }
    public static Typeface TypefaceCalibri_ItalicBold(Context context) {
        return Typeface.createFromAsset(context.getAssets(),
                "Fonts/Calibri/calibriz.ttf");
    }
}
