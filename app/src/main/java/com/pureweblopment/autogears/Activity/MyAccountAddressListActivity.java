package com.pureweblopment.autogears.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.pureweblopment.autogears.Global.Global;
import com.pureweblopment.autogears.Global.SendMail;
import com.pureweblopment.autogears.Global.SharedPreference;
import com.pureweblopment.autogears.Global.StaticUtility;
import com.pureweblopment.autogears.Global.Typefaces;
import com.pureweblopment.autogears.Model.AddressList;
import com.pureweblopment.autogears.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;
import okhttp3.Response;

public class MyAccountAddressListActivity extends AppCompatActivity implements
        View.OnClickListener {

    Context context = MyAccountAddressListActivity.this;

    //Widget
    ImageView imgBack;
    TextView txtCatName;
    static RecyclerView recyclerviewAddress;
    TextView txtNoAddressAvailable;
    Button btnAddAddress;
    RelativeLayout relativeProgress;

    String addressType = "", addressTypeName = "", txtAddressName = "", ActivityType = "";

    boolean isRefresh = false;

    static ArrayList<AddressList> addressLists = new ArrayList<>();
    LinearLayout llToolbar;

    //Internet Alert
    public EventBus eventBus = EventBus.getDefault();
    public static int i = 0;
    public static AlertDialog internetAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        setContentView(R.layout.activity_my_account_address_list);

        eventBus.register(this);

        ProgressBar progress = (ProgressBar) findViewById(R.id.progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progress.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
        }

        Initialization();
        TypeFace();
        OnClickListener();
        setDynamicString();

        chanageButton(btnAddAddress);


        Bundle bundle = getIntent().getBundleExtra("BundleAddress");
        addressType = bundle.getString("AddressType");
        addressTypeName = bundle.getString("AddressTypeName");
        ActivityType = bundle.getString("ActivityType");
        addressLists = (ArrayList<AddressList>) bundle.getSerializable("AddressList");

        if (addressLists != null) {
            if (addressLists.size() > 0) {
                txtNoAddressAvailable.setVisibility(View.GONE);
                AdapterAddress adapterAddress = new AdapterAddress(context, addressLists);
                recyclerviewAddress.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                recyclerviewAddress.setAdapter(adapterAddress);
            } else {
                getAddressList();
            }
        } else {
            getAddressList();
        }

    }

    //region setDynamicString
    private void setDynamicString() {
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO_ADDRESS_AVAILABLE) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO_ADDRESS_AVAILABLE).equals("")) {
                txtNoAddressAvailable.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sNO_ADDRESS_AVAILABLE));
            } else {
                txtNoAddressAvailable.setText(getString(R.string.no_address_available));
            }
        } else {
            txtNoAddressAvailable.setText(getString(R.string.no_address_available));
        }
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_ADDRESS) != null) {
            btnAddAddress.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADD_ADDRESS));
        } else {
            btnAddAddress.setText(getString(R.string.add_address));
        }
    }//endregion

    //region For EventBus onEvent
    public void onEvent(String event) {
        openInternetAlertDialog(context, event);
    }
    //endregion

    //region FOR SHOW INTERNET CONNECTION DIALOG...
    public void openInternetAlertDialog(final Context mContext, String alertString) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View row = inflater.inflate(R.layout.row_alert_dialog, null);
        final TextView tvAlertText = row.findViewById(R.id.tvAlertText);
        final TextView tvTitle = row.findViewById(R.id.tvTitle);
        final Button btnSettings = row.findViewById(R.id.btnSettings);
        final Button btnExit = row.findViewById(R.id.btnExit);

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF).equals("")) {
                tvTitle.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDATE_OFF));
            } else {
                tvTitle.setText(getText(R.string.your_data_is_off));
            }
        } else {
            tvTitle.setText(getText(R.string.your_data_is_off));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI).equals("")) {
                tvAlertText.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sTURN_ON_DATA_WIFI));
            } else {
                tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
            }
        } else {
            tvAlertText.setText(getText(R.string.turn_on_data_or_wi_fi_in_nsettings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS).equals("")) {
                btnSettings.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSETTINGS));
            } else {
                btnSettings.setText(getText(R.string.settings));
            }
        } else {
            btnSettings.setText(getText(R.string.settings));
        }

        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT).equals("")) {
                btnExit.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEXIT));
            } else {
                btnExit.setText(getText(R.string.exit));
            }
        } else {
            btnExit.setText(getText(R.string.exit));
        }

        tvTitle.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        tvAlertText.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
        btnSettings.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        btnExit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        btnExit.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        btnSettings.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        tvTitle.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));


        tvAlertText.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        tvTitle.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnSettings.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        btnExit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

        try {
            if (alertString.equals("Not connected to Internet")) {
                if (i == 0) {
                    i = 1;
                    AlertDialog.Builder i_builder = new AlertDialog.Builder(mContext);
                    internetAlert = i_builder.create();
                    internetAlert.setCancelable(false);
                    internetAlert.setView(row);

                    if (internetAlert.isShowing()) {
                        internetAlert.dismiss();
                    } else {
                        internetAlert.show();
                    }

                    btnExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            internetAlert.dismiss();
                            //FOR CLOSE APP...
                            System.exit(0);
                        }
                    });

                    btnSettings.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            /*internetAlert.dismiss();*/
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
                } else {
                    /*internetAlert.dismiss();*/
                }
            } else {
                i = 0;
                internetAlert.dismiss();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region ON ACTIVITY RESULT FOR DISMISS OR SHOW INTERNET ALERT DIALOG...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (!Global.isNetworkAvailable(context)) {
                openInternetAlertDialog(context, "Not connected to Internet");
            } else {
                internetAlert.dismiss();
            }
        }
    }
    //endregion

    //region Initialization
    private void Initialization() {
        recyclerviewAddress = findViewById(R.id.recyclerviewAddress);
        btnAddAddress = findViewById(R.id.btnAddAddress);
        relativeProgress = findViewById(R.id.relativeProgress);
        imgBack = findViewById(R.id.imgBack);
        txtCatName = findViewById(R.id.txtCatName);
        txtNoAddressAvailable = findViewById(R.id.txtNoAddressAvailable);
        txtNoAddressAvailable.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        txtCatName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ADDRESS) != null) {
            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ADDRESS).equals("")) {
                txtCatName.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sMY_ADDRESS));
            } else {
                txtCatName.setText(getString(R.string.myaddress));
            }
        } else {
            txtCatName.setText(getString(R.string.myaddress));
        }
        llToolbar = findViewById(R.id.llToolbar);
        llToolbar.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        imgBack.setColorFilter(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
    }
    //endregion

    //region TypeFace
    private void TypeFace() {
        txtNoAddressAvailable.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
        txtCatName.setTypeface(Typefaces.TypefaceCalibri_bold(context));
    }
    //endregion

    //region OnClickListener
    private void OnClickListener() {
        btnAddAddress.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }
    //endregion

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAddAddress:
                isRefresh = true;
                Intent intent = new Intent(context, ADDAddressesActivity.class);
                intent.putExtra("ActivityType", ActivityType);
                intent.putExtra("Type", "MyAccountAddressListActivity");
                startActivity(intent);
                finish();
                break;

            case R.id.imgBack:
                onBackPressed();
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SharedPreference.GetPreference(context, Global.LOGIN_PREFERENCE, Global.USERID) != null) {
            getAddressList();
        } else {
            txtNoAddressAvailable.setVisibility(View.VISIBLE);
        }
    }

    //region FOR getAddressList API..
    private void getAddressList() {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = new String[]{};
        String[] val = new String[]{};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.AddressList);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equals("ok")) {
                                    JSONArray jsonArrayPayload = response.getJSONArray("payload");
                                    addressLists = new ArrayList<>();
                                    if (jsonArrayPayload.length() > 0) {
                                        txtNoAddressAvailable.setVisibility(View.GONE);
                                        for (int i = 0; i < jsonArrayPayload.length(); i++) {
                                            JSONObject jsonObjectPayload = jsonArrayPayload.getJSONObject(i);
                                            String Fname = jsonObjectPayload.getString("first_name");
                                            String Lname = jsonObjectPayload.getString("last_name");
                                            String address = jsonObjectPayload.getString("address");
                                            String pincode = jsonObjectPayload.getString("pincode");
                                            String city = jsonObjectPayload.getString("city");
                                            String state = jsonObjectPayload.getString("state");
                                            String phone_number = jsonObjectPayload.getString("phone_number");
                                            String useradress_id = jsonObjectPayload.getString("useradress_id");
                                            String landmark = jsonObjectPayload.getString("landmark");
                                            String country = jsonObjectPayload.getString("country");
                                            String address_type = jsonObjectPayload.getString("address_type");
                                            addressLists.add(new AddressList(Fname, Lname, address, city, pincode, state, phone_number, useradress_id, landmark, country, address_type));
                                        }
                                        if (addressLists.size() > 0) {
                                            txtNoAddressAvailable.setVisibility(View.GONE);
                                            AdapterAddress adapterAddress = new AdapterAddress(context, addressLists);
                                            recyclerviewAddress.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                                            recyclerviewAddress.setAdapter(adapterAddress);
                                        } else {
                                            txtNoAddressAvailable.setVisibility(View.VISIBLE);
                                        }
                                    } else {
                                        txtNoAddressAvailable.setVisibility(View.VISIBLE);
                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    MainActivity.manageBackPress(true);
                                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED) != null) {
                                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED).equals("")) {
                                            Toast.makeText(context, SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    }
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*finish();*/
                                } else if (strMessage.equals("app-id and app-secret is required")) {
                                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED) != null) {
                                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED).equals("")) {
                                            Toast.makeText(context, SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sLOGIN_REQUIRED), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    }
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    intent.putExtra("Redirect", "myaccount");
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in AddressListFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    //region ADAPTER
    public class AdapterAddress extends RecyclerView.Adapter<AdapterAddress.Viewholder> {

        Context context;
        ArrayList<AddressList> addressLists = new ArrayList<>();

        public AdapterAddress(Context context, ArrayList<AddressList> addressLists) {
            this.context = context;
            this.addressLists = addressLists;
        }

        @Override
        public AdapterAddress.Viewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = null;
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_address_items, viewGroup, false);
            return new AdapterAddress.Viewholder(view);
        }

        @Override
        public void onBindViewHolder(Viewholder holder, final int position) {
            final AddressList addressList = addressLists.get(position);

            holder.UserName.setText(Catpital(addressList.getFirstName()) + " " + Catpital(addressList.getLastName()));
            holder.txtAddress.setText(addressList.getAddress() + "," + "\n" +
                    addressList.getCity() + " - " + addressList.getPincode() + "," + "\n" + addressList.getState());
            holder.txtPhoneNo.setText(addressList.getPhone_number());

            holder.txtEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String FName = addressList.getFirstName();
                    String LName = addressList.getLastName();
                    String Address = addressList.getAddress();
                    String Landmark = addressList.getLandmark();
                    String Pincode = addressList.getPincode();
                    String Country = addressList.getCountry();
                    String State = addressList.getState();
                    String City = addressList.getCity();
                    String Phoneno = addressList.getPhone_number();
                    String useradress_id = addressList.getUserAddressID();

                    Intent intent = new Intent(context, EditAddressesActivity.class);
                    intent.putExtra("ActivityType", ActivityType);
                    intent.putExtra("Fname", FName);
                    intent.putExtra("Lname", LName);
                    intent.putExtra("Address", Address);
                    intent.putExtra("Landmark", Landmark);
                    intent.putExtra("Pincode", Pincode);
                    intent.putExtra("Country", Country);
                    intent.putExtra("State", State);
                    intent.putExtra("City", City);
                    intent.putExtra("Phoneno", Phoneno);
                    intent.putExtra("addressType", addressType);
                    intent.putExtra("useradress_id", useradress_id);
                    intent.putExtra("Position", String.valueOf(position));
                    startActivity(intent);
                }
            });

            holder.txtDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String useradress_id = addressList.getUserAddressID();
                    String address_type = addressList.getAddress_type();
                    DeleteAddresses(useradress_id, address_type, position);
                }
            });

            /*if (ActivityType.equals("MainActivity")) {
                holder.cartAddress.setEnabled(false);
            } else if (ActivityType.equalsIgnoreCase("CheckoutFragment")) {
                holder.cartAddress.setEnabled(true);
            } else {
                holder.cartAddress.setEnabled(false);
            }*/
        }

        @Override
        public int getItemCount() {
            return addressLists.size();
        }

        public class Viewholder extends RecyclerView.ViewHolder {
            TextView UserName, txtEdit, txtDelete, txtAddress, txtPhoneNo;
            CardView cartAddress;

            public Viewholder(View itemView) {
                super(itemView);
                txtPhoneNo = itemView.findViewById(R.id.txtPhoneNo);
                UserName = itemView.findViewById(R.id.UserName);
                txtEdit = itemView.findViewById(R.id.txtEdit);
                txtDelete = itemView.findViewById(R.id.txtDelete);
                txtAddress = itemView.findViewById(R.id.txtAddress);
                cartAddress = (CardView) itemView.findViewById(R.id.cartAddress);

                UserName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtEdit.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtDelete.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtAddress.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
                txtPhoneNo.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEDIT) != null) {
                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEDIT).equals("")) {
                        txtEdit.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sEDIT));
                    } else {
                        txtEdit.setText(context.getString(R.string.edit));
                    }
                } else {
                    txtEdit.setText(context.getString(R.string.edit));
                }

                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELETE) != null) {
                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELETE).equals("")) {
                        txtDelete.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELETE));
                    } else {
                        txtDelete.setText(context.getString(R.string.delete));
                    }
                } else {
                    txtDelete.setText(context.getString(R.string.delete));
                }

                UserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtEdit.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtDelete.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
                txtPhoneNo.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

            }
        }
    }
    //endregion

    //region FOR DeleteAddresses API..
    private void DeleteAddresses(String useradress_id, final String address_type, final int position) {
        relativeProgress.setVisibility(View.VISIBLE);

        String[] key = {"useradress_id", "address_type"};
        String[] val = {useradress_id, address_type};

        final ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(StaticUtility.URL +
                StaticUtility.DeleteAddress);
        postRequestBuilder.addJSONObjectBody(Global.bodyParameter(key, val))
                .addHeaders(Global.headers1(context));
        ANRequest anRequest = postRequestBuilder.setTag(this)
                .setPriority(Priority.LOW)
                .setExecutor(Executors.newSingleThreadExecutor())
                .build();

        anRequest.setAnalyticsListener(new AnalyticsListener() {
            @Override
            public void onReceived(long timeTakenInMillis, long bytesSent, long bytesReceived, boolean isFromCache) {
            }
        });

        anRequest.getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {

            @Override
            public void onResponse(Response okHttpResponse, final JSONObject response) {
                Log.d("Test", "onResponse object : " + response.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                if (okHttpResponse.isSuccessful()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                if (strStatus.equalsIgnoreCase("ok")) {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                    addressLists.remove(position);
                                    recyclerviewAddress.getAdapter().notifyDataSetChanged();
                                    if (addressLists.size() <= 0) {
                                        txtNoAddressAvailable.setVisibility(View.VISIBLE);
                                        isRefresh = true;
                                        Intent intent = new Intent(context, ADDAddressesActivity.class);
                                        intent.putExtra("ActivityType", ActivityType);
                                        intent.putExtra("Type", "MyAccountAddressListActivity");
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        txtNoAddressAvailable.setVisibility(View.GONE);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }

            @Override
            public void onError(final ANError anError) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        /*itemVendorStocks.clear();*/
                        relativeProgress.setVisibility(View.GONE);
                    }
                });
                Log.d("Test", "onResponse object : " + anError.toString());
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (anError.getErrorBody() != null) {
                                JSONObject response = new JSONObject(anError.getErrorBody());
                                String strCode = response.getString("code");
                                String strStatus = response.getString("status");
                                String strMessage = response.getString("message");
                                strMessage = strMessage.replace("]", "").replace("[", "").replace("\"", "");
                                if (strCode.equalsIgnoreCase("401")) {
                                    Toast.makeText(context, getString(R.string.login_required), Toast.LENGTH_SHORT).show();
                                    SharedPreference.ClearPreference(context, Global.LOGIN_PREFERENCE);
                                    SharedPreference.ClearPreference(context, Global.WishlistCountPreference);
                                    SharedPreference.ClearPreference(context, Global.Shipping_Preference);
                                    SharedPreference.ClearPreference(context, Global.Billing_Preference);
                                    Intent intent = new Intent(context, LoginActivity.class);
                                    startActivity(intent);
                                    /*finish();*/
                                } else {
                                    Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Creating SendMail object
                            SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT, "Getting error in AddressListFragment.java When parsing Error response.\n" + anError.toString());
                            //Executing sendmail to send email
                            sm.execute();
                        }
                    }
                });
            }

        });
    }
    //endregion

    public static void EditAddresses(String Fname, String Lname, String Address, String Landmark, String Pincode, String Country,
                                     String State, String City, String Phoneno, String addressType, String useradress_id, String Position) {
        AddressList addressList = addressLists.get(Integer.parseInt(Position));
        addressList.setFirstName(Fname);
        addressList.setLastName(Lname);
        addressList.setAddress(Address);
        addressList.setLandmark(Landmark);
        addressList.setPincode(Pincode);
        addressList.setCountry(Country);
        addressList.setState(State);
        addressList.setCity(City);
        addressList.setPhone_number(Phoneno);
        recyclerviewAddress.getAdapter().notifyDataSetChanged();
    }

    public String Catpital(String strText) {
        String cap = "";
        String[] strArray = strText.split(" ");
        for (String s : strArray) {
            cap = s.substring(0, 1).toUpperCase() + s.substring(1);
        }
        return cap;
    }

    //region chanageButton
    public void chanageButton(Button button) {
        button.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        button.setBackgroundResource(R.drawable.ic_button);
        GradientDrawable gd = (GradientDrawable) button.getBackground().getCurrent();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
//        gd.setCornerRadii(new float[]{50, 50, 50, 50, 50, 50, 50, 50});
        gd.setCornerRadius(70);
        /* gd.setStroke(2, Color.parseColor(StaticUtility.BORDERCOLOR));*/
    }
    //endregion
}
