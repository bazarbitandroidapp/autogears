package com.pureweblopment.autogears.Adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pureweblopment.autogears.Global.Global;
import com.pureweblopment.autogears.Global.SendMail;
import com.pureweblopment.autogears.Global.SharedPreference;
import com.pureweblopment.autogears.Global.StaticUtility;
import com.pureweblopment.autogears.Global.Typefaces;
import com.pureweblopment.autogears.Model.OrderHistoryItems;
import com.pureweblopment.autogears.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by divya on 20/12/17.
 */

public class AdapterOrderHistory extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<OrderHistoryItems> orderHistoryItemses;
    String strPaymentType;

    public AdapterOrderHistory(Context context, ArrayList<OrderHistoryItems> orderHistoryItemses, String strPaymentType) {
        this.orderHistoryItemses = orderHistoryItemses;
        this.context = context;
        this.strPaymentType = strPaymentType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_orderhistory, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder paletteViewHolder = (ViewHolder) holder;
            if (SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    paletteViewHolder.pbImgHolder.setIndeterminateTintList(ColorStateList.valueOf(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor))));
                }
            }
            final OrderHistoryItems orderHistoryItems = (OrderHistoryItems) orderHistoryItemses.get(position);
            paletteViewHolder.txtProductName.setText(orderHistoryItems.getName());
            paletteViewHolder.txtProducDate.setText(Global.changeDateFormate(orderHistoryItems.getOrder_datetime(),
                    "yyyy-MM-dd hh:mm:ss", "dd-MMM-yyyy"));

            float floatPrice = Float.parseFloat(orderHistoryItems.getOrder_product_amount());
            int intPrice = Math.round(floatPrice);
            String image = orderHistoryItems.getImage();
            String image1 = image.replace("[", "").replace("]", "").replace("\"", "");

            String str1 = orderHistoryItems.getCurrency_symbol() + " " +
                    orderHistoryItems.getOrder_product_amount();
            String str2 = orderHistoryItems.getOrder_product_amount() + " " +
                    orderHistoryItems.getCurrency_symbol();
            if (orderHistoryItems.getCurrency_position().equals("1")) {
                paletteViewHolder.txtProductPrice.setText(str1);
            } else if (orderHistoryItems.getCurrency_position().equals("0")) {
                paletteViewHolder.txtProductPrice.setText(str2);
            }

            String order_item_status = orderHistoryItems.getOrder_item_status();

            if (order_item_status.equals("0")) {
                if (Integer.parseInt(strPaymentType) > 1) {
                    if (order_item_status.equalsIgnoreCase("0") &&
                            orderHistoryItems.getOrder_item_payment_status().
                                    equalsIgnoreCase("0")) {

                        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER) != null) {
                            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER).equals("")) {
                                paletteViewHolder.txtProductStatus.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER));
                            } else {
                                paletteViewHolder.txtProductStatus.setText(context.getString(R.string.cancelled_order));
                            }
                        } else {
                            paletteViewHolder.txtProductStatus.setText(context.getString(R.string.cancelled_order));
                        }
                        paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#FF0000"));
                        paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                    } else {
                        if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPENDING_ORDER) != null) {
                            if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPENDING_ORDER).equals("")) {
                                paletteViewHolder.txtProductStatus.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPENDING_ORDER));
                            } else {
                                paletteViewHolder.txtProductStatus.setText(context.getString(R.string.pending_order));
                            }
                        } else {
                            paletteViewHolder.txtProductStatus.setText(context.getString(R.string.pending_order));
                        }
                        paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#FF7F50"));
                        paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                    }
                } else {
                    if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPENDING_ORDER) != null) {
                        if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPENDING_ORDER).equals("")) {
                            paletteViewHolder.txtProductStatus.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPENDING_ORDER));
                        } else {
                            paletteViewHolder.txtProductStatus.setText(context.getString(R.string.pending_order));
                        }
                    } else {
                        paletteViewHolder.txtProductStatus.setText(context.getString(R.string.pending_order));
                    }
                    paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#FF7F50"));
                    paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                }
            } else if (order_item_status.equals("1")) {
                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCONFIRM_ORDER) != null) {
                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCONFIRM_ORDER).equals("")) {
                        paletteViewHolder.txtProductStatus.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCONFIRM_ORDER));
                    } else {
                        paletteViewHolder.txtProductStatus.setText(context.getString(R.string.confirm_order));
                    }
                } else {
                    paletteViewHolder.txtProductStatus.setText(context.getString(R.string.confirm_order));
                }
                paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#28b62c"));
                paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
            } else if (order_item_status.equals("2")) {
                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sIN_PROCESS) != null) {
                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sIN_PROCESS).equals("")) {
                        paletteViewHolder.txtProductStatus.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sIN_PROCESS));
                    } else {
                        paletteViewHolder.txtProductStatus.setText(context.getString(R.string.in_process));
                    }
                } else {
                    paletteViewHolder.txtProductStatus.setText(context.getString(R.string.in_process));
                }
                paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#d4af37"));
            } else if (order_item_status.equals("3")) {
                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sOUT_FOR_DELIVERY) != null) {
                if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sOUT_FOR_DELIVERY).equals("")) {
                    paletteViewHolder.txtProductStatus.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sOUT_FOR_DELIVERY));
                }else {
                    paletteViewHolder.txtProductStatus.setText(context.getString(R.string.out_for_delivery));
                }
                } else {
                    paletteViewHolder.txtProductStatus.setText(context.getString(R.string.out_for_delivery));
                }
                paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#88b04b"));
            } else if (order_item_status.equals("4")) {
                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERED) != null) {
                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERED).equals("")) {
                        paletteViewHolder.txtProductStatus.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sDELIVERED));
                    } else {
                        paletteViewHolder.txtProductStatus.setText(context.getString(R.string.delivered));
                    }
                } else {
                    paletteViewHolder.txtProductStatus.setText(context.getString(R.string.delivered));
                }
                paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#076f30"));
                paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
            } else if (order_item_status.equals("5")) {
                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER) != null) {
                    if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER).equals("")) {
                        paletteViewHolder.txtProductStatus.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCANCELLED_ORDER));
                    } else {
                        paletteViewHolder.txtProductStatus.setText(context.getString(R.string.cancelled_order));
                    }
                } else {
                    paletteViewHolder.txtProductStatus.setText(context.getString(R.string.cancelled_order));
                }
                paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#FF0000"));
                paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
            } else if (order_item_status.equals("6")) {
                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURNED_ORDER) != null) {
                if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURNED_ORDER).equals("")) {
                    paletteViewHolder.txtProductStatus.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sRETURNED_ORDER));
                }else {
                    paletteViewHolder.txtProductStatus.setText(context.getString(R.string.return_order));
                }
                } else {
                    paletteViewHolder.txtProductStatus.setText(context.getString(R.string.return_order));
                }
                paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#158cba"));
            } else if (order_item_status.equals("7")) {
                if (SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREFUND_ORDER) != null) {
                if (!SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREFUND_ORDER).equals("")) {
                    paletteViewHolder.txtProductStatus.setText(SharedPreference.GetPreference(context, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREFUND_ORDER));
                }else {
                    paletteViewHolder.txtProductStatus.setText(context.getString(R.string.refund_order));
                }
                } else {
                    paletteViewHolder.txtProductStatus.setText(context.getString(R.string.refund_order));
                }
                paletteViewHolder.txtProductStatus.setTextColor(Color.parseColor("#ffffff"));
                paletteViewHolder.txtProductStatus.setBackgroundColor(Color.parseColor("#72c29b"));
            }

            //region Image
            String picUrl = null;
            try {
                URL urla = null;
                urla = new URL(image.replaceAll("%20", " "));
                URI urin = new URI(urla.getProtocol(), urla.getUserInfo(), urla.getHost(), urla.getPort(),
                        urla.getPath(), urla.getQuery(), urla.getRef());
                picUrl = String.valueOf(urin.toURL());
                // Capture position and set to the ImageView
                Picasso.get()
                        .load(picUrl)
                        .into(paletteViewHolder.imageProduct, new Callback() {
                            @Override
                            public void onSuccess() {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                                paletteViewHolder.rlImgHolder.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception e) {
                                //holder.pbHome.setVisibility(View.INVISIBLE);
                            }
                        });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                        "Getting error in AdapterOrderHistory.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                //Creating SendMail object
                SendMail sm = new SendMail(context, Global.TOEMAIL, Global.SUBJECT,
                        "Getting error in AdapterOrderHistory.java When parsing url\n" + e.toString());
                //Executing sendmail to send email
                sm.execute();
            }
            //endregion
        }
    }

    @Override
    public int getItemCount() {
        return orderHistoryItemses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageProduct;
        TextView txtProductName, txtOrderID, txtProductPrice, txtProductStatus, txtProducDate;
        LinearLayout llOrderhistory;
        TextView txtProductOrderID;
        RelativeLayout rlImgHolder;
        ProgressBar pbImgHolder;

        public ViewHolder(View itemView) {
            super(itemView);
            txtProductName = itemView.findViewById(R.id.txtProductName);
            txtProductOrderID = itemView.findViewById(R.id.txtProductOrderID);
            txtOrderID = itemView.findViewById(R.id.txtOrderID);
            txtProductPrice = itemView.findViewById(R.id.txtProductPrice);
            txtProductStatus = itemView.findViewById(R.id.txtProductStatus);
            txtProducDate = itemView.findViewById(R.id.txtProducDate);
            imageProduct = itemView.findViewById(R.id.imageProduct);
            llOrderhistory = itemView.findViewById(R.id.llOrderhistory);
            rlImgHolder = itemView.findViewById(R.id.rlImgHolder);
            pbImgHolder = itemView.findViewById(R.id.pbImgHolder);

            txtProductName.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
            txtOrderID.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
            txtProductPrice.setTypeface(Typefaces.TypefaceCalibri_bold(context));
            txtProductStatus.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
            txtProducDate.setTypeface(Typefaces.TypefaceCalibri_Regular(context));
            txtProductOrderID.setTypeface(Typefaces.TypefaceCalibri_Regular(context));

            txtProductName.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtProductOrderID.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtOrderID.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtProducDate.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextLightColor)));
            txtProductPrice.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtProductStatus.setTextColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            txtProductStatus.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(context, Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));

        }
    }
}
