package com.pureweblopment.autogears.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pureweblopment.autogears.Global.Global;
import com.pureweblopment.autogears.Global.SharedPreference;
import com.pureweblopment.autogears.Global.StaticUtility;
import com.pureweblopment.autogears.Global.Typefaces;
import com.pureweblopment.autogears.Model.Commission;
import com.pureweblopment.autogears.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class CommissionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    ArrayList<Commission> commissions;
    DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public CommissionAdapter(Context mContext, ArrayList<Commission> commissions) {
        this.mContext = mContext;
        this.commissions = commissions;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_commission, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            Commission commission = commissions.get(i);
            viewHolder.mTxtUserName.setText(commission.getUserName());
            if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_ID) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_ID).equals("")) {
                    viewHolder.mTxtOrderID.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sORDER_ID)
                            + " :- " + commission.getOrder_id());
                } else {
                    viewHolder.mTxtOrderID.setText(mContext.getString(R.string.order_id) + " " + commission.getOrder_id());
                }
            } else {
                viewHolder.mTxtOrderID.setText(mContext.getString(R.string.order_id) + " " + commission.getOrder_id());
            }
            viewHolder.mTxtPercentage.setText(decimalFormat.format(Float.parseFloat(commission.getCommissionPercentage())));
            if (SharedPreference.GetPreference(mContext, Global.PREFERENCECURRENCY,
                    StaticUtility.sCurrencySignPosition) != null) {
                if (SharedPreference.GetPreference(mContext, Global.PREFERENCECURRENCY,
                        StaticUtility.sCurrencySignPosition).equals("1")) {
                    viewHolder.mTxtAmount.setText(SharedPreference.GetPreference(mContext,
                            Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign) + " " + decimalFormat.format(Float.parseFloat(commission.getCommissionAmount())));
                }else {
                    viewHolder.mTxtAmount.setText(decimalFormat.format(Float.parseFloat(commission.getCommissionAmount()))
                            + " " + SharedPreference.GetPreference(mContext,
                            Global.PREFERENCECURRENCY, StaticUtility.sCurrencySign));
                }
            }else {
                viewHolder.mTxtAmount.setText(decimalFormat.format(Float.parseFloat(commission.getCommissionAmount())));
            }
            if (!commission.getCommissionOkReason().equalsIgnoreCase("") &&
                    !commission.getCommissionRejectReason().equalsIgnoreCase("")) {
                viewHolder.mTxtReason.setVisibility(View.VISIBLE);
                viewHolder.mTxtReason.setText(commission.getCommissionRejectReason());
                if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREJECTED) != null) {
                    if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREJECTED).equals("")) {
                        viewHolder.mTxtStatus.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREJECTED));
                    } else {
                        viewHolder.mTxtStatus.setText(mContext.getString(R.string.rejected));
                    }
                } else {
                    viewHolder.mTxtStatus.setText(mContext.getString(R.string.rejected));
                }
                viewHolder.mTxtStatus.setBackgroundColor(Color.parseColor("#d9534f"));
            } else if (!commission.getCommissionOkReason().equalsIgnoreCase("")) {
                viewHolder.mTxtReason.setVisibility(View.VISIBLE);
                viewHolder.mTxtReason.setText(commission.getCommissionOkReason());
                if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUCCESS) != null) {
                    if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUCCESS).equals("")) {
                        viewHolder.mTxtStatus.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUCCESS));
                    } else {
                        viewHolder.mTxtStatus.setText(mContext.getString(R.string.success));
                    }
                } else {
                    viewHolder.mTxtStatus.setText(mContext.getString(R.string.success));
                }
                viewHolder.mTxtStatus.setBackgroundColor(Color.parseColor("#5cb85c"));

            } else if (!commission.getCommissionRejectReason().equalsIgnoreCase("")) {
                viewHolder.mTxtReason.setVisibility(View.VISIBLE);
                viewHolder.mTxtReason.setText(commission.getCommissionRejectReason());
                if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREJECTED) != null) {
                    if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREJECTED).equals("")) {
                        viewHolder.mTxtStatus.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sREJECTED));
                    } else {
                        viewHolder.mTxtStatus.setText(mContext.getString(R.string.rejected));
                    }
                } else {
                    viewHolder.mTxtStatus.setText(mContext.getString(R.string.rejected));
                }
                viewHolder.mTxtStatus.setBackgroundColor(Color.parseColor("#d9534f"));

            } else {
                viewHolder.mTxtReason.setVisibility(View.GONE);
                if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPENDING) != null) {
                    if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPENDING).equals("")) {
                        viewHolder.mTxtStatus.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPENDING));
                    } else {
                        viewHolder.mTxtStatus.setText(mContext.getString(R.string.pending));
                    }
                } else {
                    viewHolder.mTxtStatus.setText(mContext.getString(R.string.pending));
                }
                viewHolder.mTxtStatus.setBackgroundColor(Color.parseColor("#f0ad4e"));
            }


        }
    }

    @Override
    public int getItemCount() {
        return commissions.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTxtUserName, mTxtOrderID, mTxtPercentageLabel,
                mTxtPercentage, mTxtAmountLabel, mTxtAmount, mTxtReason, mTxtStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            mTxtUserName = itemView.findViewById(R.id.txtUserName);
            mTxtOrderID = itemView.findViewById(R.id.txtOrderID);
            mTxtPercentageLabel = itemView.findViewById(R.id.txtPercentageLabel);
            mTxtPercentage = itemView.findViewById(R.id.txtPercentage);
            mTxtAmountLabel = itemView.findViewById(R.id.txtAmountLabel);
            mTxtAmount = itemView.findViewById(R.id.txtAmount);
            mTxtReason = itemView.findViewById(R.id.txtReason);
            mTxtStatus = itemView.findViewById(R.id.txtStatus);

            mTxtUserName.setTypeface(Typefaces.TypefaceCalibri_bold(mContext));
            mTxtOrderID.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
            mTxtPercentageLabel.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
            mTxtPercentage.setTypeface(Typefaces.TypefaceCalibri_bold(mContext));
            mTxtAmountLabel.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
            mTxtAmount.setTypeface(Typefaces.TypefaceCalibri_bold(mContext));
            mTxtReason.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));
            mTxtStatus.setTypeface(Typefaces.TypefaceCalibri_Regular(mContext));

            mTxtUserName.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            mTxtOrderID.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            mTxtPercentageLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            mTxtPercentage.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            mTxtAmountLabel.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            mTxtAmount.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));
            mTxtReason.setTextColor(Color.parseColor(SharedPreference.GetPreference(mContext, Global.APPSetting_PREFERENCE, StaticUtility.TextColor)));

            if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOMMISSION_PERCENTAGE) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOMMISSION_PERCENTAGE).equals("")) {
                    mTxtPercentageLabel.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOMMISSION_PERCENTAGE));
                } else {
                    mTxtPercentageLabel.setText(mContext.getString(R.string.commission_percentage));
                }
            } else {
                mTxtPercentageLabel.setText(mContext.getString(R.string.commission_percentage));
            }
            if (SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOMMISSION_AMOUNT) != null) {
                if (!SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOMMISSION_AMOUNT).equals("")) {
                    mTxtAmountLabel.setText(SharedPreference.GetPreference(mContext, StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sCOMMISSION_AMOUNT));
                } else {
                    mTxtAmountLabel.setText(mContext.getString(R.string.commission_amount));
                }
            } else {
                mTxtAmountLabel.setText(mContext.getString(R.string.commission_amount));
            }
        }
    }
}
