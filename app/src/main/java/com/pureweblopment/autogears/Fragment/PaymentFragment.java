package com.pureweblopment.autogears.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pureweblopment.autogears.Activity.MainActivity;
import com.pureweblopment.autogears.Global.Global;
import com.pureweblopment.autogears.Global.SharedPreference;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import com.pureweblopment.autogears.Global.StaticUtility;
import com.pureweblopment.autogears.Global.Typefaces;
import com.pureweblopment.autogears.R;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PaymentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PaymentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PaymentFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private WebView webView;
    private String User_Id, mFirstName, mEmailId, mPhone, mAmount;


    private String mMerchantKey = "";//For merchant and salt key you need to contact payu money tech support otherwise you get error
    private String mSalt = "";//copy and paste works fine
    //private String mBaseURL = "https://test.payu.in/_payment";
    //private String mBaseURL = "https://test.payu.in";
    private String mAction = "https://test.payu.in/_payment"; // For Final Local URL
    //    private String mAction = "https://secure.payu.in/_payment"; // For Live Final URL
    private String mTXNId; // This will create below randomly
    private String mHash; // This will create below randomly
    private String mudf1, mudf2, mudf3;
    private String mProductInfo; // From Previous Activity
    private String mServiceProvider = "payu_paisa";
    private String mSuccessUrl;
    private String msuccessurl;
    private String mfailedurl;
    private String mFailedUrl;
    ImageView imgCompletedPaymane;
    ImageView imgCompletedSuccess, imgCompletedAddress;
    LinearLayout llPaymentHeader, llBottomNavigation;

    Handler mHandler = new Handler();
    Boolean value = true;
    public static String status = "";

    ImageView imageCartBack, imageNavigation, imageLogo;
    FrameLayout frameLayoutCart;
    TextView txtCatName;
    CardView cardviewBottomNavigation;

    private ProgressDialog progressDialog;
    TextView txtAddress, txtPayment, txtSuccess;

    private OnFragmentInteractionListener mListener;

    public PaymentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PaymentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PaymentFragment newInstance(String param1, String param2) {
        PaymentFragment fragment = new PaymentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @SuppressLint({"JavascriptInterface", "WrongConstant"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MainActivity.manageBackPress(false);
        MainActivity.checkback = true;
        View view = inflater.inflate(R.layout.fragment_payment, container, false);

        progressBarVisibilityPayuChrome(View.VISIBLE);
        webView = view.findViewById(R.id.payumoney_webview);

        imageCartBack = getActivity().findViewById(R.id.imageCartBack);
        imageNavigation = getActivity().findViewById(R.id.imageNavigation);
        imageLogo = getActivity().findViewById(R.id.imageLogo);

        frameLayoutCart = getActivity().findViewById(R.id.frameLayoutCart);
        txtCatName = getActivity().findViewById(R.id.txtCatName);
        llBottomNavigation = getActivity().findViewById(R.id.llBottomNavigation);
        cardviewBottomNavigation = getActivity().findViewById(R.id.cardviewBottomNavigation);
        txtAddress = view.findViewById(R.id.txtAddress);
        txtPayment = view.findViewById(R.id.txtPayment);
        txtSuccess = view.findViewById(R.id.txtSuccess);
        txtAddress.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        txtPayment.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        txtSuccess.setTypeface(Typefaces.TypefaceCalibri_bold(getContext()));
        txtAddress.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtPayment.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        txtSuccess.setTextColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ButtonTextColor)));
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS) != null) {
        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS).equals("")) {
            txtAddress.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sADDRESS));
        }else {
            txtAddress.setText(getString(R.string.address));
        }
        } else {
            txtAddress.setText(getString(R.string.address));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPAYMENT) != null) {
        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPAYMENT).equals("")) {
            txtPayment.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPAYMENT));
        }else {
            txtPayment.setText(getString(R.string.payment));
        }
        } else {
            txtPayment.setText(getString(R.string.payment));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUCCESS) != null) {
        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUCCESS).equals("")) {
            txtSuccess.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sSUCCESS));
        }else {
            txtSuccess.setText(getString(R.string.success));
        }
        } else {
            txtSuccess.setText(getString(R.string.success));
        }
        if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPAYMENT_INFORMATION) != null) {
        if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPAYMENT_INFORMATION).equals("")) {
            txtCatName.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPAYMENT_INFORMATION));
        }else {
            txtCatName.setText(R.string.payment_information);
        }
        } else {
            txtCatName.setText(R.string.payment_information);
        }


        imageCartBack.setVisibility(View.VISIBLE);
        txtCatName.setVisibility(View.VISIBLE);

        imageNavigation.setVisibility(View.GONE);
        imageLogo.setVisibility(View.GONE);
        frameLayoutCart.setVisibility(View.GONE);
//        llBottomNavigation.setVisibility(View.GONE);
        cardviewBottomNavigation.setVisibility(View.GONE);
        imageCartBack.setVisibility(View.GONE);

        User_Id = SharedPreference.GetPreference(getActivity(), Global.LOGIN_PREFERENCE, Global.USERID);

        llPaymentHeader = (LinearLayout) view.findViewById(R.id.llPaymentHeader);
        if (SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor) != "") {
            llPaymentHeader.setBackgroundColor(Color.parseColor(SharedPreference.GetPreference(getContext(), Global.APPSetting_PREFERENCE, StaticUtility.ThemePrimaryColor)));
        }
        Bundle bundle = getArguments();
        if (bundle != null) {
            /*mFirstName = bundle.getString("name");
            mEmailId = bundle.getString("email");
            mProductInfo = bundle.getString("productInfo");
            mAmount = Double.parseDouble(bundle.getString("amount"));//in my case amount getting as String so i parse it double
            mPhone = bundle.getString("phone");
            mId = bundle.getInt("id");
            isFromOrder = bundle.getBoolean("isFromOrder");
            */
           /* mFirstName="gaurang";
            mEmailId="gaurang.kela@gmail.com";
            mProductInfo="product1";
            //mAmount=200;
            mPhone="9033334060";
           // mId=1;
            //isFromOrder=true;*/
            imgCompletedSuccess = view.findViewById(R.id.imgCompletedSuccess);
            imgCompletedAddress = view.findViewById(R.id.imgCompletedAddress);
            imgCompletedAddress.setVisibility(View.VISIBLE);
            imgCompletedPaymane = view.findViewById(R.id.imgCompletedPaymane);

            mFirstName = bundle.getString("firstname");
            mEmailId = "";
            mPhone = bundle.getString("phone");
            mMerchantKey = bundle.getString("mMerchantKey");
            mSalt = bundle.getString("mSalt");

            mAmount = bundle.getString("amount");
            mSuccessUrl = bundle.getString("Payumoneysuccessurl");
            mFailedUrl = bundle.getString("payumoneyfailureurl");
            msuccessurl ="http://192.168.0.110/bazzarbitFront/thankyou";
            mfailedurl ="http://192.168.0.110/bazzarbitFront/transactionfailed";
            /*mSuccessUrl = "http://192.168.0.112/www.fancyshop.com/payment-response.php";
            mFailedUrl = "http://192.168.0.112/www.fancyshop.com/payment-response.php";*/

            mudf1 = bundle.getString("order_id");
            mudf2 = User_Id;
            mudf3 = SharedPreference.GetPreference(getActivity(), Global.LOGIN_PREFERENCE, Global.USERTOKEN);
            Log.i("Log", mFirstName + " : " + mEmailId + " : " + mAmount + " : " + mPhone);

            /**
             * Creating Transaction Id
             */
            Random rand = new Random();
            String randomString = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
            mTXNId = hashCal("SHA-256", randomString).substring(0, 20);


            //mAmount = new BigDecimal(mAmount).setScale(0, RoundingMode.UP).intValue();

            /**
             * Creating Hash Key
             */
            mHash = hashCal("SHA-512", mMerchantKey + "|" +
                    mTXNId + "|" +
                    mAmount + "|" +
                    mProductInfo + "|" +
                    mFirstName + "|" +
                    mEmailId + "|" +
                    mudf1 + "|" +
                    mudf2 + "|" +
                    mudf3 + "||||||||" +
                    mSalt);

            /**
             * Final Action URL...
             */
            //mAction = mBaseURL.concat("/_payment");

            /**
             * WebView Client
             */
            webView.setWebViewClient(new WebViewClient() {

                @Override
                public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                    super.onReceivedError(view, request, error);
                    Toast.makeText(getActivity(), "Oh no! " + error, Toast.LENGTH_LONG).show();
                }

                /*@Override
                public void onReceivedSslError(WebView view,
                                               SslErrorHandler handler, SslError error) {
                    Toast.makeText(getActivity(), "SSL Error! " + error, Toast.LENGTH_LONG).show();
                    handler.proceed();
                }*/

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return super.shouldOverrideUrlLoading(view, url);
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    if (url.equals(msuccessurl)) {
                        //Toast.makeText(getActivity(), "Success Url", Toast.LENGTH_LONG).show();
                        //Toast.makeText(getActivity(), url, Toast.LENGTH_LONG).show();
                        MainActivity.checkback = true;
                        imgCompletedAddress.setVisibility(View.VISIBLE);
                        imgCompletedPaymane.setVisibility(View.VISIBLE);
                        status = "Transaction Successfully!";
                        mListener = (OnFragmentInteractionListener) getContext();
                        mListener.gotoOrderSuccess("2", status);
                    } else if (url.equals(mfailedurl)) {
                        //Toast.makeText(getActivity(), "Failed Url", Toast.LENGTH_LONG).show();
                        //Toast.makeText(getActivity(), url, Toast.LENGTH_LONG).show();
                        MainActivity.checkback = true;
                        imgCompletedAddress.setVisibility(View.VISIBLE);
                        imgCompletedPaymane.setVisibility(View.GONE);
                        status = "Transaction Cancelled!";
                        mListener = (OnFragmentInteractionListener) getContext();
                        mListener.gotoOrderSuccess("2", status);
                    }
                    /**
                     * wait 10 seconds to dismiss payu money processing dialog in my case
                     */
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressBarVisibilityPayuChrome(View.GONE);
                        }
                    }, 10000);

                    super.onPageFinished(view, url);
                }
            });

            webView.setVisibility(View.VISIBLE);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setCacheMode(2);
            webView.getSettings().setDomStorageEnabled(true);
            webView.clearHistory();
            webView.clearCache(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setSupportZoom(true);
            webView.getSettings().setUseWideViewPort(false);
            webView.getSettings().setLoadWithOverviewMode(false);
            webView.addJavascriptInterface(new PayUJavaScriptInterface(getActivity()), "PayUMoney");

            /**
             * Mapping Compulsory Key Value Pairs
             */
            Map<String, String> mapParams = new HashMap<>();
            mapParams.put("key", mMerchantKey);
            mapParams.put("txnid", mTXNId);
            mapParams.put("amount", String.valueOf(mAmount));
            mapParams.put("productinfo", mProductInfo);
            mapParams.put("firstname", mFirstName);
            mapParams.put("email", mEmailId);
            mapParams.put("phone", mPhone);
            mapParams.put("surl", mSuccessUrl);
            mapParams.put("furl", mFailedUrl);
            mapParams.put("hash", mHash);
            mapParams.put("udf1", mudf1);
            mapParams.put("udf2", mudf2);
            mapParams.put("udf3", mudf3);
            mapParams.put("service_provider", mServiceProvider);

            webViewClientPost(webView, mAction, mapParams.entrySet());

        } else {
            Toast.makeText(getActivity(), "Something went wrong, Try again.", Toast.LENGTH_LONG).show();
        }
        return view;
    }

    public void progressBarVisibilityPayuChrome(int visibility) {
        try {
            if (getApplicationContext() != null && !getActivity().isFinishing()) {
                if (visibility == View.GONE || visibility == View.INVISIBLE) {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                } else if (progressDialog == null || !progressDialog.isShowing()) {
                    progressDialog = showProgress(getActivity());
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public ProgressDialog showProgress(Context context) {
        if (getApplicationContext() != null && !getActivity().isFinishing()) {
            LayoutInflater mInflater = LayoutInflater.from(context);
            final Drawable[] drawables = {getResources().getDrawable(R.drawable.l_icon1),
                    getResources().getDrawable(R.drawable.l_icon2),
                    getResources().getDrawable(R.drawable.l_icon3),
                    getResources().getDrawable(R.drawable.l_icon4)
            };

            View layout = mInflater.inflate(R.layout.prog_dialog, null);
            final ImageView imageView;
            imageView = layout.findViewById(R.id.imageView);
            final TextView mDialog_title = layout.findViewById(R.id.dialog_title);
            final TextView mDialog_desc = layout.findViewById(R.id.dialog_desc);
            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPROCESS) != null) {
                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPROCESS).equals("")) {
                    mDialog_title.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPROCESS));
                } else {
                    mDialog_title.setText(getText(R.string.processing_your_request));
                }
            } else {
                mDialog_title.setText(getText(R.string.processing_your_request));
            }

            if (SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPLEASE_WAIT) != null) {
                if (!SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPLEASE_WAIT).equals("")) {
                    mDialog_desc.setText(SharedPreference.GetPreference(getContext(), StaticUtility.MULTILANGUAGEPREFERENCE, StaticUtility.sPLEASE_WAIT));
                } else {
                    mDialog_desc.setText(getText(R.string.please_wait));
                }
            } else {
                mDialog_desc.setText(getText(R.string.please_wait));
            }
            ProgressDialog progDialog = new ProgressDialog(context, android.R.style.Theme_Holo_Light_Dialog);

            final Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                int i = -1;

                @Override
                synchronized public void run() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            i++;
                            if (i >= drawables.length) {
                                i = 0;
                            }
                            imageView.setImageBitmap(null);
                            imageView.destroyDrawingCache();
                            imageView.refreshDrawableState();
                            imageView.setImageDrawable(drawables[i]);
                        }
                    });

                }
            }, 0, 500);

            progDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    timer.cancel();
                }
            });
            progDialog.show();
            progDialog.setContentView(layout);
            progDialog.setCancelable(true);
            progDialog.setCanceledOnTouchOutside(false);
            return progDialog;
        }
        return null;
    }

    public String hashCal(String type, String str) {
        byte[] hashSequence = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        try {
            MessageDigest algorithm = MessageDigest.getInstance(type);
            algorithm.reset();
            algorithm.update(hashSequence);
            byte messageDigest[] = algorithm.digest();

            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() == 1)
                    hexString.append("0");
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException NSAE) {
        }
        return hexString.toString();
    }

    public void webViewClientPost(WebView webView, String url, Collection<Map.Entry<String, String>> postData) {
        StringBuilder sb = new StringBuilder();
        sb.append("<html><head></head>");
        sb.append("<body onload='form1.submit()'>");
        sb.append(String.format("<form id='form1' action='%s' method='%s'>", url, "post"));
        for (Map.Entry<String, String> item : postData) {
            sb.append(String.format("<input name='%s' type='hidden' value='%s' />", item.getKey(), item.getValue()));
        }
        sb.append("</form></body></html>");
        Log.d("TAG", "webViewClientPost called: " + sb.toString());
        webView.loadData(sb.toString(), "text/html", "utf-8");
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void gotoOrderSuccess(String payment_method, String urlStatus);
    }

    public class PayUJavaScriptInterface {
        Context mContext;

        PayUJavaScriptInterface(Context c) {
            mContext = c;
        }

        public void success(long id, final String paymentId) {
            Toast.makeText(getActivity(), "Payment Successfully.", Toast.LENGTH_LONG).show();
            Toast.makeText(getActivity(), paymentId, Toast.LENGTH_LONG).show();
            mHandler.post(new Runnable() {

                public void run() {

                    mHandler = null;
                    Toast.makeText(getActivity(), "Payment Successfully.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), MainActivity.class);

                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    intent.putExtra("result", "success");

                    intent.putExtra("paymentId", paymentId);

                    startActivity(intent);

                    getActivity().finish();

                }

            });

        }
    }
}
